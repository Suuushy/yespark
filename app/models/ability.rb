class Ability
  include CanCan::Ability

  def initialize(user)

    #Creation d'un compte visiteur
    user ||= User.new

    can :manage, Article

    cannot [:edit, :delete, :update], Article do |a|
        a.user != user
    end

  end
end
