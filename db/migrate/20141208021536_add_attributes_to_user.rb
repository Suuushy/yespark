class AddAttributesToUser < ActiveRecord::Migration
  def self.up
  	add_column :users, :firstname, :string
  	add_column :users, :lastname, :string
  	add_column :users, :admin, :boolean, default: false
  	add_column :users, :moderator, :boolean, default: false
  end
  
  def self.down
  	remove_column :users, :firstname
  	remove_column :users, :lastname
  	remove_column :users, :admin
  	remove_column :users, :moderator
  end
end
