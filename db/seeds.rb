puts "###### Seeding started #########"
puts ""

# Initialisation

User.destroy_all
Article.destroy_all
Comment.destroy_all

# Creation de comptes utilisateur

puts "###### Accounts created #########"
puts "###### All passwords set to changeme ########"
puts ""
puts "###### Email available : jean@yopmail.com #########"
puts "###### Email available : pierre@yopmail.com #########"
puts "###### Email available : paul@yopmail.com #########"
puts ""

u = []
u << User.create({firstname: 'Jean', lastname: 'Bernard', email: 'jean@yopmail.com', password: 'changeme', password_confirmation: 'changeme'})
u << User.create({firstname: 'Pierre', lastname: 'Laloi', email: 'pierre@yopmail.com', password: 'changeme', password_confirmation: 'changeme'})
u << User.create({firstname: 'Paul', lastname: 'Henri', email: 'paul@yopmail.com', password: 'changeme', password_confirmation: 'changeme'})

# Creation d'articles type

Article.create({title: 'Article de Jean', content: 'Ceci est le premier article généré par défaut pour l utilisateur Jean', user_id: u[0].id})
Article.create({title: 'Article de Pierre', content: 'Ceci est un autre article généré pour l utilisateur Pierre', user_id: u[1].id})
